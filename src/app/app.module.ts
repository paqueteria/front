import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from './layout/layout.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandlerInterceptor } from './interceptor/error-handler.interceptor';
import { registerLocaleData } from '@angular/common';
import localeEsMX from '@angular/common/locales/es-MX';
import { SharedComponentsModule } from './shared-components/shared-components.module';
import { PageNotFoundComponent } from './containers/page-not-found/page-not-found.component';
import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material/dialog';
import { ApiInterceptor } from './interceptor/api.interceptor';
import { EmpleadosModule } from './empleados/empleados.module';

// registrar los locales con el nombre que quieras utilizar a la hora de proveer
registerLocaleData(localeEsMX, 'es');

@NgModule({
  declarations: [AppComponent, PageNotFoundComponent],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedComponentsModule,
    AppRoutingModule,
    LayoutModule,
    EmpleadosModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorHandlerInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiInterceptor,
      multi: true,
    },
    { provide: LOCALE_ID, useValue: 'es' },
    {
      provide: MAT_DIALOG_DEFAULT_OPTIONS,
      useValue: { disableClose: true, hasBackdrop: true, width: '500px' },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
