export enum PaqueteStatus {
  PaqueteCancelado = -1,
  PaqueteAlmacenado = 0,
  PaquetePreparado = 1,
  PaqueteAsignado = 2,
  PaqueteEnCamino = 3,
  PaqueteEntregado = 4,
}
