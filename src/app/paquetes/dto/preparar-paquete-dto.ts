export class PrepararPaqueteDto {
  id: string;
  domicilio: string;
  nombreDestinatario: string;
  tipoServicio: string;
}
