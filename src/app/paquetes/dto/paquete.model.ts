export class Paquete {
  _id: string;
  titulo: string;
  descripcion: string;
  fechaCreacion: Date;
  status: number;
  tamanio: string;
  empleadoCreoId: string;
}
