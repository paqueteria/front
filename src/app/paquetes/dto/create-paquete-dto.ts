export class CreatePaqueteDto {
  titulo: string;
  descripcion: string;
  tamanio: string;
  empleadoCreoId?: string;
}
