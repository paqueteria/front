import { Component, OnInit } from '@angular/core';
import { PaquetesService } from './paquetes.service';
import { Paquete } from './dto/paquete.model';
import { PAQUETES_TABS, TODOS_PAQUETES_TAB_OPTION } from './paquetes.constants';
import { MatDialog } from '@angular/material/dialog';
import { AddPaqueteDialogComponent } from './containers/add-paquete-dialog/add-paquete-dialog.component';
import { CreatePaqueteDto } from './dto/create-paquete-dto';
import { MatTabChangeEvent } from '@angular/material/tabs';
import { LoginService } from '../login/login.service';
import { UsuarioRol } from '../empleados/enum/usuario-rol';
import { UpdatePaqueteDto } from './dto/update-paquete.dto';
@Component({
  selector: 'app-paquetes',
  templateUrl: './paquetes.component.html',
  styles: [],
})
export class PaquetesComponent implements OnInit {
  paquetes: Paquete[] = [];
  tabIndex: number = 0;

  constructor(
    private paquetesService: PaquetesService,
    public dialog: MatDialog,
    public loginService: LoginService,
  ) {}

  ngOnInit() {
    if (
      this.loginService.getRol() === UsuarioRol.Chofer ||
      this.loginService.getRol() === UsuarioRol.ChoferElite
    ) {
      this.paquetesService
        .getPaquetesAsignados(this.loginService.getUsuarioId())
        .subscribe((paquetes: Paquete[]) => (this.paquetes = paquetes));
    } else {
      this.paquetesService
        .getAllPaquetes()
        .subscribe((paquetes: Paquete[]) => (this.paquetes = paquetes));
    }
  }

  async onClickAddPaquete() {
    const dialogRef = this.dialog.open(AddPaqueteDialogComponent);
    dialogRef.afterClosed().subscribe((paquete: CreatePaqueteDto) => {
      this.paquetesService
        .createPaquete(paquete)
        .subscribe((paqueteCreado: Paquete) => {
          this.paquetes.push(paqueteCreado);
          this.paquetes = [...this.paquetes];
        });
    });
  }

  async onChangeTab(event: MatTabChangeEvent) {
    this.tabIndex = event.index;
    if (this.tabIndex == TODOS_PAQUETES_TAB_OPTION) {
      this.paquetesService
        .getAllPaquetes()
        .subscribe((paquetes: Paquete[]) => (this.paquetes = paquetes));
    } else {
      this.paquetesService
        .getPaquetesByStatus(PAQUETES_TABS[this.tabIndex].value)
        .subscribe((paquetes: Paquete[]) => {
          this.paquetes = paquetes;
        });
    }
  }

  updatePaquete(paqueteUpdate: UpdatePaqueteDto) {
    this.paquetes = this.paquetes.map((p: Paquete) => {
      if (p._id == paqueteUpdate._id) {
        p.status = paqueteUpdate.status;
        return p;
      } else {
        return p;
      }
    });
  }

  onAsignarPaquetes() {
    this.paquetes = [];
  }
}
