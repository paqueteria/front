import { PaqueteStatus } from './enum/paquete-status.enum';
import {
  PAQUETES_TABLE_COLUMNS_AlMACENADOS,
  PAQUETES_TABLE_COLUMNS_TODOS,
  PAQUETES_TABLE_COLUMNS_PREPARADOS,
  PAQUETES_TABLE_COLUMNS_EN_CAMINO,
  PAQUETES_TABLE_COLUMNS_ENTREGADOS,
  PAQUETES_TABLE_COLUMNS_ASIGNADOS,
  PAQUETES_TABLE_COLUMNS_CHOFER,
} from './containers/paquetes-table/paquetes-table.constants';

export const PAQUETE_STATUS_VALUES = {
  '-1': { nombre: 'Cancelado', color: 'red' },
  '0': { nombre: 'Almacenado', color: 'gray' },
  '1': { nombre: 'Preparado', color: 'purple' },
  '2': { nombre: 'Asignado', color: 'pink' },
  '3': { nombre: 'En camino', color: 'orange' },
  '4': { nombre: 'Entregado', color: 'green' },
};

export const PAQUETE_TAMANIO_VALUES = ['Grande', 'Mediano', 'Chico'];

export const PAQUETE_TIPO_SERVICIO_VALUES = ['Normal', 'Elite'];

export const PAQUETES_TABS = {
  '0': {
    value: '',
    tableColumns: PAQUETES_TABLE_COLUMNS_TODOS,
  },
  '1': {
    value: PaqueteStatus.PaqueteAlmacenado,
    tableColumns: PAQUETES_TABLE_COLUMNS_AlMACENADOS,
  },
  '2': {
    value: PaqueteStatus.PaquetePreparado,
    tableColumns: PAQUETES_TABLE_COLUMNS_PREPARADOS,
  },
  '3': {
    value: PaqueteStatus.PaqueteAsignado,
    tableColumns: PAQUETES_TABLE_COLUMNS_ASIGNADOS,
  },
  '4': {
    value: PaqueteStatus.PaqueteEnCamino,
    tableColumns: PAQUETES_TABLE_COLUMNS_EN_CAMINO,
  },
  '5': {
    value: PaqueteStatus.PaqueteEntregado,
    tableColumns: PAQUETES_TABLE_COLUMNS_ENTREGADOS,
  },
  '6': {
    value: PaqueteStatus.PaqueteEntregado,
    tableColumns: PAQUETES_TABLE_COLUMNS_CHOFER,
  },
};

export const TODOS_PAQUETES_TAB_OPTION = 0;
