import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Paquete } from './dto/paquete.model';
import { CreatePaqueteDto } from './dto/create-paquete-dto';
import { BASE_API_URL } from '../app.constants';
import { PrepararPaqueteDto } from './dto/preparar-paquete-dto';
import { UpdatePaqueteDto } from './dto/update-paquete.dto';

@Injectable({
  providedIn: 'root',
})
export class PaquetesService {
  constructor(private http: HttpClient) {}

  getAllPaquetes() {
    return this.http.get<Paquete[]>(`${BASE_API_URL}/api/paquetes`);
  }

  getPaqueteById(paqueteId: string) {
    return this.http.get<Paquete[]>(
      `${BASE_API_URL}/api/paquetes/${paqueteId}`,
    );
  }

  createPaquete(paquete: CreatePaqueteDto) {
    return this.http.post(`${BASE_API_URL}/api/paquetes`, paquete);
  }

  getPaquetesByStatus(status: number) {
    return this.http.get<Paquete[]>(
      `${BASE_API_URL}/api/paquetes?status=${status}`,
    );
  }

  getPaquetesAsignados(choferAsignadoId: string) {
    return this.http.get<Paquete[]>(
      `${BASE_API_URL}/api/paquetes?choferAsignado=${choferAsignadoId}`,
    );
  }

  prepararPaquete(paquete: PrepararPaqueteDto) {
    return this.http.post(`${BASE_API_URL}/api/paquetes/preparar`, paquete);
  }

  asignarPaquetes() {
    return this.http.get(`${BASE_API_URL}/api/paquetes/asignar-paquetes`);
  }

  update(updatePaqueteDto: UpdatePaqueteDto) {
    return this.http.patch(`${BASE_API_URL}/api/paquetes`, updatePaqueteDto);
  }
}
