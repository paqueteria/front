import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { PAQUETE_TAMANIO_VALUES } from '../../paquetes.constants';
import { CreatePaqueteDto } from '../../dto/create-paquete-dto';

@Component({
  selector: 'app-add-paquete-dialog',
  templateUrl: './add-paquete-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `,
  ],
})
export class AddPaqueteDialogComponent {
  paquete: CreatePaqueteDto = new CreatePaqueteDto();
  tamanioOptions: string[] = PAQUETE_TAMANIO_VALUES;
  constructor(public dialogRef: MatDialogRef<AddPaqueteDialogComponent>) {}

  onCancelarClick(): void {
    this.dialogRef.close(this.paquete);
  }
  onClickAddPaquete() {
    this.dialogRef.close(this.paquete);
  }
}
