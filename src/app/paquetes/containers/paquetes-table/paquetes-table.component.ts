import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Paquete } from '../../dto/paquete.model';
import { PAQUETES_TABS } from '../../paquetes.constants';
import { MatDialog } from '@angular/material/dialog';
import { PrepararPaqueteDialogComponent } from '../preparar-paquete-dialog/preparar-paquete-dialog.component';
import { PrepararPaqueteDto } from '../../dto/preparar-paquete-dto';
import { PaquetesService } from '../../paquetes.service';
import { LoginService } from 'src/app/login/login.service';
import { UpdatePaqueteDto } from '../../dto/update-paquete.dto';
import { PaqueteStatus } from '../../enum/paquete-status.enum';

@Component({
  selector: 'paquetes-table',
  templateUrl: './paquetes-table.component.html',
  styles: [
    `
      table {
        width: 100%;
      }
    `,
  ],
})
export class PaquetesTableComponent {
  constructor(
    public dialog: MatDialog,
    private paquetesService: PaquetesService,
    public loginService: LoginService,
  ) {}
  paqueteStatus = PaqueteStatus;
  loading = false;
  columns: string[];
  _tabIndex: number;
  @Input()
  paquetes: Paquete[];
  @Input()
  set tabIndex(tabIndex: number) {
    this.columns = PAQUETES_TABS[tabIndex].tableColumns;
    this._tabIndex = tabIndex;
  }
  @Output() onUpdatePaquete = new EventEmitter<UpdatePaqueteDto>();

  @Output() onAsignarPaquetes = new EventEmitter<any>();

  async onClickPrepararPaquete(paqueteSeleccionado: Paquete) {
    const dialogRef = this.dialog.open(PrepararPaqueteDialogComponent);
    dialogRef.afterClosed().subscribe((paquete: PrepararPaqueteDto) => {
      paquete.id = paqueteSeleccionado._id;
      this.paquetesService.prepararPaquete(paquete).subscribe(response => {
        this.borrarPaquete(paquete.id);
      });
    });
  }

  borrarPaquete(paqueteId: string) {
    this.paquetes = this.paquetes.filter(paquete => paquete._id !== paqueteId);
  }

  asignarPaquetes() {
    this.loading = true;
    this.paquetesService.asignarPaquetes().subscribe(response => {
      this.loading = false;
      this.onAsignarPaquetes.emit('');
    });
  }

  onEnCamino(paqueteId: string) {
    const updatePaquete = new UpdatePaqueteDto();
    updatePaquete.status = PaqueteStatus.PaqueteEnCamino;
    updatePaquete._id = paqueteId;
    this.onUpdatePaquete.emit(updatePaquete);
    this.paquetesService.update(updatePaquete).subscribe(response => {
      this.loading = false;
    });
  }

  onEntregado(paqueteId: string) {
    const updatePaquete = new UpdatePaqueteDto();
    updatePaquete._id = paqueteId;
    updatePaquete.status = PaqueteStatus.PaqueteEntregado;
    this.onUpdatePaquete.emit(updatePaquete);
    this.paquetesService.update(updatePaquete).subscribe(response => {
      this.loading = false;
    });
  }
}
