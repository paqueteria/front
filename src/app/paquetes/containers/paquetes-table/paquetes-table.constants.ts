export const PAQUETES_TABLE_COLUMNS_TODOS: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'fechaCreacion',
  'status',
];

export const PAQUETES_TABLE_COLUMNS_AlMACENADOS: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'fechaCreacion',
  'select',
];

export const PAQUETES_TABLE_COLUMNS_PREPARADOS: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'fechaCreacion',
  'nombreDestinatario',
  'domicilio',
  'tipoServicio',
];

export const PAQUETES_TABLE_COLUMNS_CHOFER: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'nombreDestinatario',
  'domicilio',
  'tipoServicio',
  'status',
  'opcion',
];

export const PAQUETES_TABLE_COLUMNS_ASIGNADOS: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'nombreDestinatario',
  'domicilio',
  'tipoServicio',
  'choferAsignado',
];

export const PAQUETES_TABLE_COLUMNS_EN_CAMINO: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'nombreDestinatario',
  'domicilio',
  'tipoServicio',
  'choferAsignado',
];

export const PAQUETES_TABLE_COLUMNS_ENTREGADOS: string[] = [
  'titulo',
  'descripcion',
  'tamanio',
  'fechaCreacion',
];
