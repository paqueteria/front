import { Component, Input } from '@angular/core';
import { PAQUETE_STATUS_VALUES } from '../../paquetes.constants';

@Component({
  selector: 'paquete-status',
  template: `
    <p [color]="getColorByPaqueteStatus()">
      {{ getNameByPaqueteStatus() }}
    </p>
  `,
  styles: [],
})
export class PaqueteStatusComponent {
  @Input() paqueteStatus: number;

  getColorByPaqueteStatus(): string {
    return PAQUETE_STATUS_VALUES[this.paqueteStatus].color;
  }

  getNameByPaqueteStatus(): string {
    return PAQUETE_STATUS_VALUES[this.paqueteStatus].nombre;
  }
}
