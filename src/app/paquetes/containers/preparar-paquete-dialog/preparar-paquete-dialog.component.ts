import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { PAQUETE_TIPO_SERVICIO_VALUES } from '../../paquetes.constants';
import { PrepararPaqueteDto } from '../../dto/preparar-paquete-dto';

@Component({
  selector: 'preparar-paquete-dialog',
  templateUrl: './preparar-paquete-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `,
  ],
})
export class PrepararPaqueteDialogComponent {
  paquete: PrepararPaqueteDto = new PrepararPaqueteDto();
  tipoServiciosOptions: string[] = PAQUETE_TIPO_SERVICIO_VALUES;

  constructor(public dialogRef: MatDialogRef<PrepararPaqueteDialogComponent>) {}

  onCancelarClick(): void {
    this.dialogRef.close(this.paquete);
  }

  onClickPrepararPaquete() {
    this.dialogRef.close(this.paquete);
  }
}
