import { NgModule } from '@angular/core';
import { SharedComponentsModule } from 'src/app/shared-components/shared-components.module';
import { PaquetesTableComponent } from './paquetes-table/paquetes-table.component';
import { PaqueteStatusComponent } from './paquete-status/paquete-status.component';
import { AddPaqueteDialogComponent } from './add-paquete-dialog/add-paquete-dialog.component';
import { PrepararPaqueteDialogComponent } from './preparar-paquete-dialog/preparar-paquete-dialog.component';

@NgModule({
  imports: [SharedComponentsModule],
  declarations: [
    PaquetesTableComponent,
    PaqueteStatusComponent,
    AddPaqueteDialogComponent,
    PrepararPaqueteDialogComponent,
  ],
  exports: [
    PaquetesTableComponent,
    PaqueteStatusComponent,
    AddPaqueteDialogComponent,
    PrepararPaqueteDialogComponent,
  ],
  providers: [],
})
export class ContainersModule {}
