import { NgModule } from '@angular/core';

import { PaquetesRoutingModule } from './paquetes-routing.module';
import { PaquetesComponent } from './paquetes.component';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { ContainersModule } from './containers/containers.module';
import { AddPaqueteDialogComponent } from './containers/add-paquete-dialog/add-paquete-dialog.component';
import { PrepararPaqueteDialogComponent } from './containers/preparar-paquete-dialog/preparar-paquete-dialog.component';
import { PaquetesService } from './paquetes.service';

@NgModule({
  declarations: [PaquetesComponent],
  imports: [PaquetesRoutingModule, SharedComponentsModule, ContainersModule],
  providers: [PaquetesService],
  entryComponents: [AddPaqueteDialogComponent, PrepararPaqueteDialogComponent],
  exports: [],
})
export class PaquetesModule {}
