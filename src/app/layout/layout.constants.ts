export const MENU_SIDE_BAR_ADMIN = [
  { nombre: 'Paquetes', icon: 'markunread_mailbox', link: '/paquetes' },
  { nombre: 'Empleados', icon: 'people', link: '/empleados' },
];

export const MENU_SIDE_BAR_BODEGUERO = [
  { nombre: 'Paquetes', icon: 'markunread_mailbox', link: '/paquetes' },
];

export const MENU_SIDE_BAR_CHOFER = [
  {
    nombre: 'Paquetes a enviar',
    icon: 'markunread_mailbox',
    link: '/paquetes',
  },
];

export const MENU_SIDE_BAR_ROL = {
  Admin: MENU_SIDE_BAR_ADMIN,
  Chofer: MENU_SIDE_BAR_CHOFER,
  Bodeguero: MENU_SIDE_BAR_BODEGUERO,
  'Chofer elite': MENU_SIDE_BAR_CHOFER,
};
