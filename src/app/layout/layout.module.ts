import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './top-bar/top-bar.component';
import { LayoutComponent } from './layout.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { SharedMaterialComponentsModule } from '../shared-components/shared-material-components.module';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [SideBarComponent, TopBarComponent, LayoutComponent],
  imports: [CommonModule, SharedMaterialComponentsModule, RouterModule],
  exports: [LayoutComponent],
})
export class LayoutModule {}
