import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'top-bar',
  templateUrl: './top-bar.component.html',
  styles: [],
})
export class TopBarComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit() {}

  onClickCerrarSesion() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('usuarioId');
    localStorage.removeItem('rol');
    this.router.navigateByUrl('login');
  }
}
