export class MenuItem {
  readonly nombre: string;
  readonly icon: string;
  readonly link: string;
}
