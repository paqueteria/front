import { Component, OnInit } from '@angular/core';
import { MenuItem } from './dto/menu-item.dto';
import { Router } from '@angular/router';
import { LoginService } from '../../../app/login/login.service';
import { MENU_SIDE_BAR_ROL } from '../layout.constants';
@Component({
  selector: 'side-bar',
  templateUrl: './side-bar.component.html',
  styles: [``],
})
export class SideBarComponent implements OnInit {
  constructor(private router: Router, private loginService: LoginService) {}

  menuItems: MenuItem[];

  ngOnInit() {
    this.menuItems = MENU_SIDE_BAR_ROL[this.loginService.getRol()];
  }

  onClickMenuItem(menuItem: MenuItem) {
    this.router.navigateByUrl(menuItem.link);
  }
}
