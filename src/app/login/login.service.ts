import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BASE_API_URL } from '../app.constants';
import { LoginUsuarioDto } from './dto/login-usuario.dto';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  constructor(private http: HttpClient) {}

  login(usuarioLoginDto: LoginUsuarioDto) {
    return this.http.post(
      `${BASE_API_URL}/api/usuarios/login`,
      usuarioLoginDto,
    );
  }

  getRol(): string {
    return localStorage.getItem('rol');
  }

  getUsuarioId(): string {
    return localStorage.getItem('usuarioId');
  }
}
