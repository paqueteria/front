import { Input, Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { LoginService } from './login.service';
import { UsuarioAccessTokenDto } from './dto/usuario-access-token.dto';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  error: string;
  show = true;
  constructor(private loginService: LoginService, private router: Router) {}
  form: FormGroup = new FormGroup({
    username: new FormControl(''),
    password: new FormControl(''),
  });

  onLogin() {
    this.loginService.login(this.form.value).subscribe(
      (response: UsuarioAccessTokenDto) => {
        localStorage.setItem('access_token', response.access_token);
        localStorage.setItem('usuarioId', response.usuarioId);
        localStorage.setItem('rol', response.rol);
        this.show = false;
        this.router.navigateByUrl('/');
      },
      error => {
        this.error = 'El usuario o la contraseña son incorrectos';
      },
    );
  }
}
