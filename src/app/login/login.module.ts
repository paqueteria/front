import { NgModule } from '@angular/core';
import { LoginService } from './login.service';
import { LoginComponent } from './login.component';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { LoginRoutingModule } from './login-routing.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [LoginRoutingModule, SharedComponentsModule],
  providers: [LoginService],
  exports: [LoginComponent],
})
export class LoginModule {}
