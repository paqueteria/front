export class UsuarioAccessTokenDto {
  usuarioId: string;
  rol: string;
  access_token: string;
}
