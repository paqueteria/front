import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'floating-button',
  template: `
    <button mat-fab color="primary" (click)="clickHandler()">
      <mat-icon>add</mat-icon>
    </button>
  `,
  styles: [
    `
      button {
        position: absolute;
        right: 30px;
        bottom: 22px;
      }
    `,
  ],
})
export class FloatingButtonComponent implements OnInit {
  ngOnInit() {}
  @Output() readonly onClick = new EventEmitter();
  clickHandler() {
    this.onClick.emit('');
  }
}
