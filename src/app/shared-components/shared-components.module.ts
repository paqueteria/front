import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FloatingButtonComponent } from './floating-button/floating-button.component';
import { SharedMaterialComponentsModule } from 'src/app/shared-components/shared-material-components.module';

@NgModule({
  declarations: [FloatingButtonComponent],
  imports: [CommonModule, SharedMaterialComponentsModule],
  exports: [
    CommonModule,
    SharedMaterialComponentsModule,
    FloatingButtonComponent,
  ],
})
export class SharedComponentsModule {}
