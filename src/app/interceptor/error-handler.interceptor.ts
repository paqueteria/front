import { Injectable, ErrorHandler } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse,
} from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerInterceptor implements HttpInterceptor {
  constructor(private router: Router, private errorHandler: ErrorHandler) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      // we catch the error
      catchError((errorResponse: HttpErrorResponse) => {
        // if the status is Unauthorized
        if (errorResponse.status === 401) {
          // we redirect to login
          this.router.navigateByUrl('/login');
        } else {
          // else we notify the user
          this.errorHandler.handleError(errorResponse);
        }
        return throwError(errorResponse);
      }),
    );
  }
}
