import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from './containers/page-not-found/page-not-found.component';
import { LoggedInGuard } from './guard/logged-in.guard';
import { LoginGuard } from './guard/login.guard';

const appRoutes: Routes = [
  { path: '', redirectTo: '/paquetes', pathMatch: 'full' },
  {
    path: 'empleados',
    loadChildren: () =>
      import('./empleados/empleados.module').then(m => m.EmpleadosModule),
    runGuardsAndResolvers: 'always',
  },
  {
    path: 'paquetes',
    loadChildren: () =>
      import('./paquetes/paquetes.module').then(m => m.PaquetesModule),
    canLoad: [LoggedInGuard],
    runGuardsAndResolvers: 'always',
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginModule),
    canLoad: [LoginGuard],
    runGuardsAndResolvers: 'always',
  },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
