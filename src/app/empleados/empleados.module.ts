import { NgModule } from '@angular/core';
import { EmpleadosComponent } from './empleados.component';
import { EmpleadosRoutingModule } from './empleados-routing.module';
import { SharedComponentsModule } from '../shared-components/shared-components.module';
import { ContainersModule } from './containers/containers.module';
import { AddEmpleadoDialogComponent } from './containers/add-empleado-dialog/add-empleado-dialog.component';
import { UpdateEmpleadoDialogComponent } from './containers/update-empleado-dialog/update-empleado-dialog.component';
import { EmpleadosService } from './empleados.service';

@NgModule({
  declarations: [EmpleadosComponent],
  imports: [EmpleadosRoutingModule, SharedComponentsModule, ContainersModule],
  providers: [EmpleadosService],
  entryComponents: [AddEmpleadoDialogComponent, UpdateEmpleadoDialogComponent],
  exports: [EmpleadosComponent],
})
export class EmpleadosModule {}
