import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Usuario } from './dto/usuario.model';
import { BASE_API_URL } from '../app.constants';
import { UpdateEmpleadoDto } from './dto/update-empleado.dto';
import { CreateEmpleadoDto } from './dto/create-empleado.dto';

@Injectable({
  providedIn: 'root',
})
export class EmpleadosService {
  constructor(private http: HttpClient) {}

  getAllUsuarios() {
    return this.http.get<Usuario[]>(`${BASE_API_URL}/api/usuarios`);
  }

  isLoggedIn(): boolean {
    if (localStorage.getItem('access_token')) {
      return true;
    }
    return false;
  }

  updateEmpleado(updateEmpleadoDto: UpdateEmpleadoDto) {
    return this.http.patch<Usuario>(
      `${BASE_API_URL}/api/usuarios`,
      updateEmpleadoDto,
    );
  }

  addEmpleado(createEmpleadoDto: CreateEmpleadoDto) {
    return this.http.post<Usuario>(
      `${BASE_API_URL}/api/usuarios`,
      createEmpleadoDto,
    );
  }

  deleteEmpleado(empleadoId: string) {
    return this.http.delete<Usuario>(
      `${BASE_API_URL}/api/usuarios/${empleadoId}`,
    );
  }
}
