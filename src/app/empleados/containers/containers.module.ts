import { NgModule } from '@angular/core';
import { SharedComponentsModule } from 'src/app/shared-components/shared-components.module';
import { AddEmpleadoDialogComponent } from './add-empleado-dialog/add-empleado-dialog.component';
import { UpdateEmpleadoDialogComponent } from './update-empleado-dialog/update-empleado-dialog.component';

@NgModule({
  imports: [SharedComponentsModule],
  declarations: [AddEmpleadoDialogComponent, UpdateEmpleadoDialogComponent],
  exports: [AddEmpleadoDialogComponent, UpdateEmpleadoDialogComponent],
  providers: [],
})
export class ContainersModule {}
