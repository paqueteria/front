import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { CreateEmpleadoDto } from '../../dto/create-empleado.dto';
import { UsuarioRol } from '../../enum/usuario-rol';

@Component({
  selector: 'add-empleado-dialog',
  templateUrl: './add-empleado-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `,
  ],
})
export class AddEmpleadoDialogComponent {
  empleado: CreateEmpleadoDto = new CreateEmpleadoDto();
  rolesOptions: string[] = Object.values(UsuarioRol).filter(
    e => typeof e === 'string',
  );
  constructor(public dialogRef: MatDialogRef<AddEmpleadoDialogComponent>) {}

  onCancelarClick(): void {
    this.dialogRef.close(this.empleado);
  }
  onClickAddEmpleado() {
    this.dialogRef.close(this.empleado);
  }
}
