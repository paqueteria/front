import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UsuarioRol } from '../../enum/usuario-rol';
import { UpdateEmpleadoDto } from '../../dto/update-empleado.dto';

@Component({
  selector: 'update-empleado-dialog',
  templateUrl: './update-empleado-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
    `,
  ],
})
export class UpdateEmpleadoDialogComponent implements OnInit {
  empleado: UpdateEmpleadoDto;
  rolesOptions: string[] = Object.values(UsuarioRol).filter(
    e => typeof e === 'string',
  );
  constructor(
    public dialogRef: MatDialogRef<UpdateEmpleadoDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public dialogData: UpdateEmpleadoDto,
  ) {}

  ngOnInit(): void {
    this.empleado = this.dialogData;
  }

  onCancelarClick(): void {
    this.dialogRef.close(this.empleado);
  }
  onClickUpdateEmpleado() {
    this.dialogRef.close(this.empleado);
  }
}
