export enum UsuarioRol {
  Admin = 'Admin',
  Bodeguero = 'Bodeguero',
  Chofer = 'Chofer',
  ChoferElite = 'Chofer elite',
}
