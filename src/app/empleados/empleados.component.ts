import { Component, OnInit } from '@angular/core';
import { EMPLEADOS_COLUMNS } from './empleados.constants';
import { Usuario } from './dto/usuario.model';
import { EmpleadosService } from './empleados.service';
import { MatDialog } from '@angular/material/dialog';
import { AddEmpleadoDialogComponent } from './containers/add-empleado-dialog/add-empleado-dialog.component';
import { CreateEmpleadoDto } from './dto/create-empleado.dto';
import { UpdateEmpleadoDto } from './dto/update-empleado.dto';
import { UpdateEmpleadoDialogComponent } from './containers/update-empleado-dialog/update-empleado-dialog.component';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styles: [
    `
      table {
        width: 60%;
      }
    `,
  ],
})
export class EmpleadosComponent implements OnInit {
  columns = EMPLEADOS_COLUMNS;
  usuarios: Usuario[] = [];

  constructor(
    private empleadosService: EmpleadosService,
    public dialog: MatDialog,
  ) {}

  ngOnInit() {
    this.empleadosService
      .getAllUsuarios()
      .subscribe((usuarios: Usuario[]) => (this.usuarios = usuarios));
  }

  onClickAddEmpleado() {
    const dialogRef = this.dialog.open(AddEmpleadoDialogComponent);
    dialogRef.afterClosed().subscribe((empleado: CreateEmpleadoDto) => {
      this.empleadosService
        .addEmpleado(empleado)
        .subscribe((empleadoCreado: Usuario) => {
          this.usuarios.push(empleadoCreado);
          this.usuarios = [...this.usuarios];
        });
    });
  }

  onModificarEmpleado(empleado: Usuario) {
    const dialogRef = this.dialog.open(UpdateEmpleadoDialogComponent, {
      data: { ...empleado },
    });
    dialogRef.afterClosed().subscribe((empleado: UpdateEmpleadoDto) => {
      this.empleadosService
        .updateEmpleado(empleado)
        .subscribe((empleadoModificado: Usuario) => {
          this.usuarios = this.usuarios.map((empleado: Usuario, index) => {
            if (empleado._id === empleadoModificado._id) {
              return empleadoModificado;
            } else {
              return empleado;
            }
          });
        });
    });
  }

  onBorrarEmpleado(empleadoId: string) {
    this.empleadosService.deleteEmpleado(empleadoId).subscribe(response => {
      this.usuarios = this.usuarios.filter(
        (empleado: Usuario) => empleado._id !== empleadoId,
      );
    });
  }
}
