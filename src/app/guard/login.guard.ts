import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EmpleadosService } from '../empleados/empleados.service';

@Injectable({
  providedIn: 'root',
})
export class LoginGuard implements CanLoad {
  constructor(
    private router: Router,
    private empleadosService: EmpleadosService,
  ) {}
  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (this.empleadosService.isLoggedIn()) {
      this.router.navigateByUrl('/paquetes');
      return false;
    } else {
      return true;
    }
  }
}
