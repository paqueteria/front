import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, Router } from '@angular/router';
import { EmpleadosService } from '../empleados/empleados.service';

@Injectable({
  providedIn: 'root',
})
export class LoggedInGuard implements CanLoad {
  constructor(
    private router: Router,
    private empleadosService: EmpleadosService,
  ) {}
  canLoad(route: Route, segments: UrlSegment[]): boolean {
    if (this.empleadosService.isLoggedIn()) {
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
