import { Component } from '@angular/core';
import { EmpleadosService } from './empleados/empleados.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: [],
})
export class AppComponent {
  constructor(public empleadosService: EmpleadosService) {}
}
